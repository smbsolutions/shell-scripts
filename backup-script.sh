# echo "Enter S3 Bucket Name"
# read BUCKET_NAME

# /home/frappe/frappe-bench/
# echo "Enter ERPNext Path"
# read ERPNEXT_PATH
read -e -p "Enter ERPNext Path: Default-" -i "/home/frappe/frappe-bench/" ERPNEXT_PATH


# echo "Backup Path"
# read BACKUP_PATH
read -e -p "Backup Path: Keep client name-" BACKUP_FOLDER

# echo "Keep backup for how many days?"
# read DAYS
read -e -p "Keep backup for how many days on s3?: Default-" -i "45" DAYS

# echo "MySQL Username"
# read MYSQL_USER
read -e -p "MySQL Username: Default-" -i "root" MYSQL_USER

echo "MySQL Password"
read MYSQL_PASS

# How many days old files must be to be removed
# DAYS=30
BUCKET_NAME="smbbk"
BACKUP_PATH=/root/$BACKUP_FOLDER
# make sure backup directory exists
[ ! -d $BACKUP_PATH ] && mkdir -p ${BACKUP_PATH}
# echo "${BUCKET_NAME} - ${ERPNEXT_PATH}"

sudo apt-get install -y s3cmd


sudo tee /root/.s3cfg > /dev/null <<-EOL
[default]
access_key = AKIA6PR6CKW2IQ3K4TYZ
access_token =
add_encoding_exts =
add_headers =
bucket_location = us-east-2
ca_certs_file =
cache_file =
check_ssl_certificate = True
check_ssl_hostname = True
cloudfront_host = cloudfront.amazonaws.com
default_mime_type = binary/octet-stream
delay_updates = False
delete_after = False
delete_after_fetch = False
delete_removed = False
dry_run = False
enable_multipart = True
encoding = UTF-8
encrypt = False
expiry_date =
expiry_days =
expiry_prefix =
follow_symlinks = False
force = False
get_continue = False
gpg_command = /usr/bin/gpg
gpg_decrypt = %(gpg_command)s -d --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
gpg_encrypt = %(gpg_command)s -c --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
gpg_passphrase =
guess_mime_type = True
host_base = s3.amazonaws.com
host_bucket = %(bucket)s.s3.amazonaws.com
human_readable_sizes = False
invalidate_default_index_on_cf = False
invalidate_default_index_root_on_cf = True
invalidate_on_cf = False
kms_key =
limit = -1
limitrate = 0
list_md5 = False
log_target_prefix =
long_listing = False
max_delete = -1
mime_type =
multipart_chunk_size_mb = 15
multipart_max_chunks = 10000
preserve_attrs = True
progress_meter = True
proxy_host =
proxy_port = 0
put_continue = False
recursive = False
recv_chunk = 65536
reduced_redundancy = False
requester_pays = False
restore_days = 1
restore_priority = Standard
secret_key = HLaY7RlmGsPvgMhY39y1tyrjOO26pRNoGI9J9BSj
send_chunk = 65536
server_side_encryption = False
signature_v2 = False
signurl_use_https = False
simpledb_host = sdb.amazonaws.com
skip_existing = False
socket_timeout = 300
stats = False
stop_on_error = False
storage_class =
urlencoding_mode = normal
use_http_expect = False
use_https = True
use_mime_magic = True
verbosity = WARNING
website_endpoint = http://%(bucket)s.s3-website-%(location)s.amazonaws.com/
website_error =
website_index = index.html

EOL

sudo tee /root/dbbackup.sh > /dev/null <<-EOL
#!/bin/bash
# Shell script to backup MySQL database

# Set these variables
MyHOST="localhost"      # DB_HOSTNAME

# Email for notifications
EMAIL="jaypatel16@gmail.com"

# Linux bin paths
MYSQL="\$(which mysql)"
MYSQLDUMP="\$(which mysqldump)"
GZIP="\$(which gzip)"


# Get date in dd-mm-yyyy format
NOW="\$(date +"%d-%m-%Y")"

# Store backup path
BACKUP="$BACKUP_PATH/\$NOW"

# make sure backup directory exists
[ ! -d \$BACKUP ] && mkdir -p \${BACKUP}


# DB skip list
SKIP="information_schema
mysql
performance_schema"

# Get all databases
DBS="\$(\$MYSQL -h \$MyHOST -u$MYSQL_USER -p$MYSQL_PASS -Bse 'show databases')"


# Archive database dumps
for db in \$DBS
do
	skipdb=-1
	if [ "\$SKIP" != "" ];
	then
		for i in \$SKIP
		do
			[ "\$db" == "\$i" ] && skipdb=1 || :
		done
	fi

	if [ "\$skipdb" == "-1" ] ; then
	FILE="\$BACKUP/\$db.sql"
	\$MYSQLDUMP -h \$MyHOST -u $MYSQL_USER -p$MYSQL_PASS --single-transaction --quick --lock-tables=false \$db > \$FILE
	fi
done

# Archive the directory, send mail and cleanup
cd \$BACKUP
tar -cf db_backup.tar .
\$GZIP -9 db_backup.tar

#echo "MySQL backup is completed! Backup name is \$NOW.tar.gz" | mail -s "MySQL backup" \$EMAIL
rm -rf *.sql

cd ../../
find  $BACKUP_PATH/* -mtime +6 -exec rm -r {} \;

s3cmd ls s3://$BUCKET_NAME/$BACKUP_FOLDER/ | while read -r line;
  do
    createDate=\`echo \$line|awk {'print \$1" "\$2'}\`
    createDate=\`echo \$createDate | sed -E "s/.*\/([0-9]+)-([0-9]+)-([0-9]+)\//\3\/\2\/\1/g" |xargs -I II date -d II +%s\`
    olderThan=\`date -d"-$DAYS days" +%s\`
    if [[ \$createDate -lt \$olderThan ]]
      then
        fileName=\`echo \$line|awk {'print \$2'}\`
        if [[ \$fileName != "" ]]
          then
            echo "deleting "\$fileName
            s3cmd del -r "\$fileName"
        fi
    fi
  done;

EOL


sudo tee /root/erpnext_code_backup.sh > /dev/null <<-EOL

#!/bin/bash
# A Simple Shell Script to Backup Red Hat / CentOS / Fedora / Debian / Ubuntu Apache Webserver and SQL Database

# Get date in dd-mm-yyyy format
NOW="\$(date +"%d-%m-%Y")"

# Store backup path
BACKUP="$BACKUP_PATH/\$NOW"

# make sure backup directory exists
[ ! -d \$BACKUP ] && mkdir -p \${BACKUP}

# Backup file name hostname.time.tar.gz
BFILE="frappe.tar.gz"

# Paths for binary files
TAR="/bin/tar"
GZIP="/bin/gzip"

LOGGER="/usr/bin/logger"

# Log backup start time in /var/log/messages
$LOGGER "\$0: *** Backup started @ \$(date) ***"

# Backup websever dirs
\$TAR -zcvf \${BACKUP}/\${BFILE} "$ERPNEXT_PATH"

# Log backup end time in /var/log/messages
\$LOGGER "\$0: *** Backup Ended @ \$(date) ***"

EOL

chmod 777 /root/dbbackup.sh
chmod 777 /root/erpnext_code_backup.sh


crontab -l | { cat; echo "0 1 * * * /bin/bash /root/dbbackup.sh"; } | crontab -

crontab -l | { cat; echo "0 2 * * 5 /bin/bash /root/erpnext_code_backup.sh"; } | crontab -

crontab -l | { cat; echo "0 4 * * * s3cmd sync -v $BACKUP_PATH  s3://$BUCKET_NAME/ > s3_backup.log 2>&1 &"; } | crontab -
