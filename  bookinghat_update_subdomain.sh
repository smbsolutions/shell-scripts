#!/bin/bash
  

for d in /home/bookinghat_subdomain/*; do
  if [ -d "$d" ]; then
    echo "$d"
    DBNAME=`echo $d | cut -c28-`
    cd "$d"
    git config core.fileMode false
    cp application/config/config.php application/config/config.php.local
    cp application/config/database.php application/config/database.php.local
    git checkout application/config/config.php
    git checkout application/config/database.php
    git pull
    cp application/config/config.php.local application/config/config.php
    cp application/config/database.php.local application/config/database.php
    mysql -uroot -proot123 $DBNAME -e "source sql/bookinghat.sql"
  fi
done