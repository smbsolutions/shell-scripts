#!/bin/bash
  
echo "servername:".$1

SERVERNAME=$1
NAME=$2
USERNAME=$3
PASSWORD=$4

#cp -r /home/bookinghat_subdomain/fresh_code /home/bookinghat_subdomain/$SERVERNAME
git clone https://smbsolutions:AdminPass@gitlab.com/smbsolutions/bookinghat.git /home/bookinghat_subdomain/$SERVERNAME

mysql -u root -pBooking@smb -Bse "CREATE DATABASE IF NOT EXISTS $SERVERNAME;USE $SERVERNAME; SOURCE /home/bookinghat_subdomain/$SERVERNAME/sql/bookinghat.sql;"
mysql -u root -pBooking@smb -Bse "INSERT INTO $SERVERNAME.s1_users (userid, name, username, password, level, is_active) VALUES (NULL, '$NAME', '$USERNAME', '$PASSWORD', 'System Administrator', '1');"

chmod -R 777 /home/bookinghat_subdomain/$SERVERNAME

sed -i "s/DBNAMEVARIABLE/$SERVERNAME/g"  /home/bookinghat_subdomain/$SERVERNAME/application/config/database.php
sed -i "s/DBNAMEVARIABLE/$SERVERNAME/g"  /home/bookinghat_subdomain/$SERVERNAME/application/config/config.php

tee /etc/nginx/sites-available/$SERVERNAME.conf > /dev/null <<-EOL
server {
        listen 80;
        root /home/bookinghat_subdomain/$SERVERNAME;
        index index.php index.html index.htm;
        server_name $SERVERNAME.bookinghat.com;

        location / {
                try_files \$uri \$uri/ =404;
        }

        location ~ \\.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass php;
        }
        
        if (!-e \$request_filename) {
              rewrite ^.*$ /index.php last;
        }

        location ~ /\\.ht {
                deny all;
        }
        
        location ~ ^/(status|ping)$ {
                allow 127.0.0.1;
                fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
                fastcgi_index index.php;
                include fastcgi_params;
                #fastcgi_pass 127.0.0.1:9000;
                fastcgi_pass  php;
        }
}


EOL

service nginx reload